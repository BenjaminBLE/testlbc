package com.example.mylbctest

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.mylbctest.database.AlbumDao
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.database.PictureDao
import com.example.mylbctest.model.Album
import com.example.mylbctest.model.Picture
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DataBaseTest {
    private lateinit var pictureDao: PictureDao
    private lateinit var albumDao: AlbumDao
    private lateinit var db: AppRoomDatabase
    private val album = Album(1, "https://via.placeholder.com/150/92c952", byteArrayOf(), 50, true)
    private val albumList = arrayListOf(album,
        Album(2, "https://via.placeholder.com/150/771796", byteArrayOf(), 50, false)
    )
    private val picture = Picture(1, 1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", byteArrayOf(), "https://via.placeholder.com/150/92c952", byteArrayOf())

    @Before
    fun init () {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, AppRoomDatabase::class.java).build()
        pictureDao = db.pictureDao()
        albumDao = db.albumDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAlbumListGetAllAlbum() {
        albumDao.insertAlbum(albumList)
        val albumListFromBDD = albumDao.getAllAlbum()

        Assert.assertNotNull(albumListFromBDD)

        Assert.assertEquals(albumList[0].isChecked, albumListFromBDD[0].isChecked)
        Assert.assertEquals(albumList[0].thumbnail, albumListFromBDD[0].thumbnail)
        Assert.assertEquals(albumList[0].counterPicture, albumListFromBDD[0].counterPicture)
        Assert.assertEquals(albumList[0].albumId, albumListFromBDD[0].albumId)

        Assert.assertEquals(albumList[1].isChecked, albumListFromBDD[1].isChecked)
        Assert.assertEquals(albumList[1].thumbnail, albumListFromBDD[1].thumbnail)
        Assert.assertEquals(albumList[1].counterPicture, albumListFromBDD[1].counterPicture)
        Assert.assertEquals(albumList[1].albumId, albumListFromBDD[1].albumId)
    }

    @Test
    @Throws(Exception::class)
    fun insertAlbumGetAlbumById() {
        albumDao.insertAlbum(album)
        val albumFromBDD = albumDao.getAlbumById(album.albumId)

        Assert.assertNotNull(albumFromBDD)

        Assert.assertEquals(album.isChecked, albumFromBDD.isChecked)
        Assert.assertEquals(album.thumbnail, albumFromBDD.thumbnail)
        Assert.assertEquals(album.counterPicture, albumFromBDD.counterPicture)
        Assert.assertEquals(album.albumId, albumFromBDD.albumId)
    }

    @Test
    @Throws(Exception::class)
    fun insetAlbumUpdateAlbum() {
        albumDao.insertAlbum(album)
        album.counterPicture = 30
        albumDao.updateAlbum(album)
        val albumFromBDD = albumDao.getAlbumById(album.albumId)

        Assert.assertNotNull(albumFromBDD)

        Assert.assertEquals(album.isChecked, albumFromBDD.isChecked)
        Assert.assertEquals(album.thumbnail, albumFromBDD.thumbnail)
        Assert.assertEquals(album.counterPicture, albumFromBDD.counterPicture)
        Assert.assertEquals(album.albumId, albumFromBDD.albumId)
    }


    @Test
    @Throws(Exception::class)
    fun insertPictureGetAllPicture() {
        pictureDao.insertPicture(picture)

        val pictureListFromBdd = pictureDao.getAllPictures()
        Assert.assertNotNull(pictureListFromBdd)
        Assert.assertEquals(1, pictureListFromBdd.size)
        Assert.assertEquals(pictureListFromBdd[0].thumbnail, picture.thumbnail)
        Assert.assertEquals(pictureListFromBdd[0].picture, picture.picture)
        Assert.assertEquals(pictureListFromBdd[0].title, picture.title)
        Assert.assertEquals(pictureListFromBdd[0].albumId, picture.albumId)
        Assert.assertEquals(pictureListFromBdd[0].id, picture.id)

        val counter = pictureDao.getPictureCounter()
        Assert.assertEquals(1, counter)

        val pictureByAlbumId = pictureDao.getPicturesByAlbumId(album.albumId)
        Assert.assertNotNull(pictureByAlbumId)
        Assert.assertEquals(1, pictureByAlbumId.size)
        Assert.assertEquals(picture.thumbnail, pictureByAlbumId[0].thumbnail)
        Assert.assertEquals(picture.picture, pictureByAlbumId[0].picture)
        Assert.assertEquals(picture.id, pictureByAlbumId[0].id)
        Assert.assertEquals(picture.albumId, pictureByAlbumId[0].albumId)
        Assert.assertEquals(picture.title, pictureByAlbumId[0].title)

        val pictureById = pictureDao.getPictureById(1)
        Assert.assertNotNull(pictureById)
        Assert.assertEquals(picture.thumbnail, pictureById.thumbnail)
        Assert.assertEquals(picture.picture, pictureById.picture)
        Assert.assertEquals(picture.id, pictureById.id)
        Assert.assertEquals(picture.albumId, pictureById.albumId)
        Assert.assertEquals(picture.title, pictureById.title)
    }

}