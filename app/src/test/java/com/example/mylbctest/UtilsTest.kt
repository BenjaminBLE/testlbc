package com.example.mylbctest

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.mylbctest.model.Picture
import com.example.mylbctest.model.PictureRetrofit
import com.example.mylbctest.utils.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk=[28])
class UtilsTest {

    private lateinit var pictureRetrofitList: List<PictureRetrofit>
    private lateinit var pictureList: List<Picture>
    private lateinit var context: Context

    @Before
    fun init() {
        context = ApplicationProvider.getApplicationContext()
        pictureRetrofitList = arrayListOf(PictureRetrofit(1, 1, "accusamus beatae ad facilis cum similique qui sunt","https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952"))
        pictureList = arrayListOf(Picture(1, 1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", byteArrayOf(), "https://via.placeholder.com/150/92c952", byteArrayOf()))
    }

    @Test
    fun pictureRetrofitToPictureTest() {
        val pictureListTransform = Utils.pictureRetrofitToPicture(pictureRetrofitList)
        Assert.assertEquals(pictureListTransform[0].id, pictureList[0].id)
        Assert.assertEquals(pictureListTransform[0].albumId, pictureList[0].albumId)
        Assert.assertEquals(pictureListTransform[0].title, pictureList[0].title)
        Assert.assertEquals(pictureListTransform[0].picture, pictureList[0].picture)
        Assert.assertEquals(pictureListTransform[0].thumbnail, pictureList[0].thumbnail)
    }

    @Test
    fun bitmapToByteArrayTest() {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.thumbnail_default)

        val byteArray = Utils.bitmapToByteArray(bitmap)
        Assert.assertNotNull(byteArray)

        val bitmapFromByteArray = Utils.byteArrayToBitmap(byteArray)
        Assert.assertNotNull(bitmapFromByteArray)
    }
}