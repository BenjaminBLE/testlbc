package com.example.mylbctest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mylbctest.model.Album
import com.example.mylbctest.model.Picture

@Database(entities = [Picture::class, Album::class], version = 1, exportSchema = false)
abstract class AppRoomDatabase: RoomDatabase() {
    abstract fun pictureDao(): PictureDao
    abstract fun albumDao(): AlbumDao

    companion object {
        @Volatile
        private var INSTANCE: AppRoomDatabase? = null

        fun getInstance(context: Context): AppRoomDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                AppRoomDatabase::class.java, "app.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}