package com.example.mylbctest.database

import androidx.room.*
import com.example.mylbctest.model.Picture

@Dao
interface PictureDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPictureList(pictureList: List<Picture>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPicture(picture: Picture)

    @Query("SELECT * FROM Picture WHERE album_id = :albumId")
    fun getPicturesByAlbumId(albumId: Long): List<Picture>

    @Query("SELECT COUNT(*) FROM Picture ")
    fun getPictureCounter() : Int

    @Query("SELECT * FROM Picture")
    fun getAllPictures(): List<Picture>

    @Query("SELECT * FROM Picture WHERE id = :pictureId")
    fun getPictureById(pictureId: Long): Picture

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatePictures(pictures: List<Picture>): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun deleteBitmapByPicture(pictures: List<Picture>): Int

}