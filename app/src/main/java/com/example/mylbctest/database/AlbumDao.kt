package com.example.mylbctest.database

import androidx.room.*
import com.example.mylbctest.model.Album
import kotlinx.coroutines.DEBUG_PROPERTY_VALUE_ON

@Dao
interface AlbumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAlbum(albumList: List<Album>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAlbum(album: Album)

    @Query("SELECT * FROM album")
    fun getAllAlbum(): List<Album>

    @Query("SELECT * FROM album WHERE album_id = :id")
    fun getAlbumById(id: Long): Album

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAlbum(album: Album)
}