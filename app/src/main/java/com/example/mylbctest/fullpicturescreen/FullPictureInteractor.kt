package com.example.mylbctest.fullpicturescreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.model.Picture
import com.example.mylbctest.model.PictureToModelUi
import com.example.mylbctest.utils.Utils
import io.reactivex.rxjava3.core.Observable
import okhttp3.internal.Util

class FullPictureInteractor(private val appRoomDatabase: AppRoomDatabase): FullPictureMvpContract.Interactor {

    private var TAG = "FullPictureInteractor"

    override fun getPictureById(pictureId: Long): Observable<PictureToModelUi> {
        Log.i(TAG, "getPictureById: début de la méthode")

        return Observable.defer {

            val picture: Picture = appRoomDatabase.pictureDao().getPictureById(pictureId)

            val pictureToModelUi = PictureToModelUi(picture.id,
                picture.albumId,
                picture.title,
                picture.picture,
                Utils.byteArrayToBitmap(picture.pictureByteArray),
                picture.thumbnail,
                Utils.byteArrayToBitmap(picture.thumbnailByteArray))

            Observable.just(pictureToModelUi)

        }
    }
}