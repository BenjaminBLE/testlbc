package com.example.mylbctest.fullpicturescreen

import com.example.mylbctest.model.PictureToModelUi
import io.reactivex.rxjava3.core.Observable

interface FullPictureMvpContract {

    interface View{
        fun showPicture(pictureToModelUi: PictureToModelUi)
    }

    interface Presenter{
        fun loadPictureById(pictureId: Long)
        fun clearDisposable()
    }

    interface Interactor{
        fun getPictureById(pictureId: Long): Observable<PictureToModelUi>
    }
}