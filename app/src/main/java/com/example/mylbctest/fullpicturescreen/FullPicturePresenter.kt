package com.example.mylbctest.fullpicturescreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class FullPicturePresenter(private val view: FullPictureActivity): FullPictureMvpContract.Presenter {

    private var mView: FullPictureActivity = view
    private var disposable: CompositeDisposable = CompositeDisposable()
    private var appRoomDatabase = AppRoomDatabase.getInstance(view)
    private var mInteractor: FullPictureInteractor = FullPictureInteractor(appRoomDatabase)
    private var TAG: String = "FullPicturePresenter"


    override fun loadPictureById(pictureId: Long) {
        Log.i(TAG, "loadPictureById: début de la méthode")

        disposable.add(
            mInteractor.getPictureById(pictureId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe { pictureToModelUit ->
                    if (pictureToModelUit != null) {
                        mView.showPicture(pictureToModelUit)
                    } else {
                        Log.e(TAG, "loadPictureById: erreur dans la réucpération de l'image via l'id")
                    }
                }
        )
    }

    override fun clearDisposable() {
        disposable.dispose()
    }
}