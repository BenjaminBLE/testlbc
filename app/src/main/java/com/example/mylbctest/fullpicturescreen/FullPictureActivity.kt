package com.example.mylbctest.fullpicturescreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.mylbctest.R
import com.example.mylbctest.databinding.ActivityFullPictureBinding
import com.example.mylbctest.model.PictureToModelUi
import com.example.mylbctest.utils.Utils

class FullPictureActivity : AppCompatActivity(), FullPictureMvpContract.View {


    private lateinit var mPresenter: FullPicturePresenter
    private val TAG: String = "FullPictureActivity"
    private lateinit var binding: ActivityFullPictureBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFullPictureBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val pictureId: Long = intent.getLongExtra(Utils.PICTURE_ID, 1)

        mPresenter = FullPicturePresenter(this)

        mPresenter.loadPictureById(pictureId)

    }

    override fun showPicture(pictureToModelUi: PictureToModelUi) {

        if (pictureToModelUi.pictureBitmap == null) {
            Glide.with(this)
                .load(Utils.getGlideUrl(pictureToModelUi.picture))
                .placeholder(R.drawable.full_picture_default)
                .into(binding.imageviewFullPictureActivity)
        } else {
            Glide.with(this)
                .load(pictureToModelUi.pictureBitmap)
                .placeholder(R.drawable.full_picture_default)
                .into(binding.imageviewFullPictureActivity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearDisposable()
    }
}