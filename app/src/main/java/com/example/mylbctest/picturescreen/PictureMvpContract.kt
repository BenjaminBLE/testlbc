package com.example.mylbctest.picturescreen

import com.example.mylbctest.model.Picture
import com.example.mylbctest.model.PictureToModelUi
import io.reactivex.rxjava3.core.Observable

interface PictureMvpContract {

    interface View {
        fun showPicturesById(pictureList: List<PictureToModelUi>)
    }

    interface Presenter {
        fun loadPicturesById(albumId: Long)
        fun clearDisposable()
    }

    interface Interactor {
        fun getPicturesById(albumId: Long): Observable<List<PictureToModelUi>>
    }
}