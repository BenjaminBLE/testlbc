package com.example.mylbctest.picturescreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mylbctest.databinding.ActivityPictureBinding
import com.example.mylbctest.model.PictureToModelUi
import com.example.mylbctest.utils.Utils

class PictureActivity : AppCompatActivity(), PictureMvpContract.View {

    private lateinit var mPresenter: PicturePresenter
    private val TAG: String = "PictureActivity"
    private lateinit var binding: ActivityPictureBinding
    private lateinit var adapter: PictureAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPictureBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val albumId = intent.getLongExtra(Utils.ALBUM_ID, 1)
        val isPictureSaveInBdd = intent.getBooleanExtra(Utils.ALBUM_IS_SAVE, false)

        mPresenter = PicturePresenter(this)

        adapter = PictureAdapter(this, isPictureSaveInBdd)
        binding.recycleViewAlbumActivity.adapter = adapter



        mPresenter.loadPicturesById(albumId)
    }

    override fun showPicturesById(pictureList: List<PictureToModelUi>) {
        adapter.setData(pictureList)
        adapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearDisposable()
    }
}