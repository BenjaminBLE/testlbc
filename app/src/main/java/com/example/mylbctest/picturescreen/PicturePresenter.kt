package com.example.mylbctest.picturescreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class PicturePresenter(view: PictureActivity) : PictureMvpContract.Presenter{

    private var mView: PictureActivity = view
    private var disposable: CompositeDisposable = CompositeDisposable()
    private var appRoomDatabase = AppRoomDatabase.getInstance(view)
    private var mInteractor: PictureInteractor = PictureInteractor(appRoomDatabase)
    private var TAG: String = "PicturePresenter"

    override fun loadPicturesById(albumId: Long) {
        Log.i(TAG, "loadPicturesById: ")

        disposable.add(mInteractor.getPicturesById(albumId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { pictureList ->
                    if (pictureList.isNotEmpty()) {
                        mView.showPicturesById(pictureList)
                    } else {
                        Log.e(TAG, "loadPicturesById: Erreur lors de la récupération des pictures", )
                    }
                })
    }

    override fun clearDisposable() {
        disposable.dispose()
    }
}