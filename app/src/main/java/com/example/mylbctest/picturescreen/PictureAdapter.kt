package com.example.mylbctest.picturescreen

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mylbctest.R
import com.example.mylbctest.fullpicturescreen.FullPictureActivity
import com.example.mylbctest.model.PictureToModelUi
import com.example.mylbctest.utils.Utils


class PictureAdapter(private val activity: PictureActivity, private val isPictureSaveInBdd: Boolean) : RecyclerView.Adapter<PictureAdapter.PictureViewHolder>() {

    private var pictureList: List<PictureToModelUi> = ArrayList()

    fun setData(pictures: List<PictureToModelUi>) {
        pictureList = pictures
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cardview_picture, parent, false)
        return PictureViewHolder(view)
    }

    override fun onBindViewHolder(holder: PictureViewHolder, position: Int) {
        val picture: PictureToModelUi = pictureList[position]
        val request = Glide.with(activity)

        if (isPictureSaveInBdd && picture.pictureBitmap != null) {
            request.load(picture.thumbnailBitmap)
                    .override(410, 410)
                    .placeholder(R.drawable.thumbnail_default)
                    .error(R.drawable.thumbnail_default)
                    .into(holder.itemImage)
        } else {
            request.load(Utils.getGlideUrl(picture.thumbnail))
                    .override(410, 410)
                    .placeholder(R.drawable.thumbnail_default)
                    .error(R.drawable.thumbnail_default)
                    .into(holder.itemImage)
        }

        holder.itemTitle.text = picture.title

        holder.cardView.setOnClickListener {
            val intent = Intent(activity, FullPictureActivity::class.java)
            intent.putExtra(Utils.PICTURE_ID, picture.id)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return pictureList.size
    }



    inner class PictureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var itemImage: ImageView = itemView.findViewById(R.id.PictureImageView)
        var itemTitle: TextView = itemView.findViewById(R.id.PictureTitleTextView)
        var cardView: CardView = itemView.findViewById(R.id.PictureCardView)
    }
}