package com.example.mylbctest.picturescreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.model.PictureToModelUi
import com.example.mylbctest.utils.Utils
import io.reactivex.rxjava3.core.Observable

class PictureInteractor(appRoomDatabase: AppRoomDatabase) : PictureMvpContract.Interactor {

    private var TAG = "PictureInteractor"
    private var mDatabase = appRoomDatabase

    override fun getPicturesById(albumId: Long): Observable<List<PictureToModelUi>> {
        Log.i(TAG, "getPicturesById: début de la méthode")

        return Observable.defer {
            val pictureList = mDatabase.pictureDao().getPicturesByAlbumId(albumId)
            val pictureToModelUiList = ArrayList<PictureToModelUi>()

            if (pictureList.isNotEmpty()) {

                for (picture in pictureList) {

                    val pictureToModelUi =  PictureToModelUi(picture.id,
                            picture.albumId,
                            picture.title,
                            picture.picture,
                            Utils.byteArrayToBitmap(picture.pictureByteArray),
                            picture.thumbnail,
                            Utils.byteArrayToBitmap(picture.thumbnailByteArray))

                    pictureToModelUiList.add(pictureToModelUi)
                }

            }

            Observable.just(pictureToModelUiList)
        }
    }
}