package com.example.mylbctest.network

import com.example.mylbctest.model.PictureRetrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class PictureServices {

    interface PictureServices {

        @GET("img/shared/technical-test.json")
        fun getPictures(): Call<List<PictureRetrofit>>

        companion object {
            private const val BASE_URL = "https://static.leboncoin.fr/"

            fun create(): PictureServices {

                val logger = HttpLoggingInterceptor()
                logger.level = HttpLoggingInterceptor.Level.BASIC

                val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()

                return Retrofit.Builder()
                    .client(client)
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(PictureServices::class.java)
            }
        }
    }
}