package com.example.mylbctest.model

import android.graphics.Bitmap

data class AlbumToModelUi(
        var albumId: Long = 0,

        var thumbnail: String = "",

        var thumbnailBitmap: Bitmap?,

        var counterPicture: Long = 0,

        var isChecked: Boolean = false
)