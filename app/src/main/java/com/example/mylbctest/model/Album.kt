package com.example.mylbctest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Album(
        @ColumnInfo(name = "album_id")
        @PrimaryKey(autoGenerate = false)
        var albumId: Long = 0,

        var thumbnail: String = "",

        @ColumnInfo(name = "thumbnail_bytearray")
        var thumbnailByteArray: ByteArray? = byteArrayOf(),

        @ColumnInfo(name = "counter_picture")
        var counterPicture: Long = 0,

        @ColumnInfo(name = "is_checked")
        var isChecked: Boolean = false
)