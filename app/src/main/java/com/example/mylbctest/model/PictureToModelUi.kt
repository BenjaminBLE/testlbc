package com.example.mylbctest.model

import android.graphics.Bitmap

data class PictureToModelUi (
        var id: Long,
        val albumId: Long,
        val title: String,
        var picture: String,
        var pictureBitmap: Bitmap?,
        var thumbnail: String,
        var thumbnailBitmap: Bitmap?
        )
