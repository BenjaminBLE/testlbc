package com.example.mylbctest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Picture(
    @PrimaryKey(autoGenerate = false)
    var id: Long,
    @ColumnInfo(name = "album_id")
    val albumId: Long,
    val title: String,
    var picture: String,
    var pictureByteArray: ByteArray? = byteArrayOf(),
    var thumbnail: String,
    var thumbnailByteArray: ByteArray? = byteArrayOf()
)
