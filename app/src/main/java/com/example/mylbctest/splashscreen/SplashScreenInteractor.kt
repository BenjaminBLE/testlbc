package com.example.mylbctest.splashscreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.model.PictureRetrofit
import com.example.mylbctest.network.PictureServices
import com.example.mylbctest.utils.Utils
import io.reactivex.rxjava3.core.Observable

class SplashScreenInteractor(private val appRoomDatabase: AppRoomDatabase): SplashScreenMvpContract.Interactor {

    private val services = PictureServices.PictureServices.create()
    private val TAG: String = "SplashScreenInteractor"

    override fun loadAllPicturesInBDD(): Observable<Boolean> {

       return Observable.defer {
            Log.i(TAG, "loadAllPicturesInBDD: thread utilisé" + Thread.currentThread())

            val call = services.getPictures()
            val response = call.execute()

            if (response.isSuccessful && response.body() != null) {

                appRoomDatabase.pictureDao().insertPictureList(Utils.pictureRetrofitToPicture((response.body() as List<PictureRetrofit>)))
                Observable.just(true)
            } else {
                Observable.just(false)
            }
        }
    }

    override fun checkIfDatabaseIsEmpty(): Observable<Boolean> {
        return Observable.defer{
            Observable.just(appRoomDatabase.pictureDao().getPictureCounter() != 0)
        }
    }

}