package com.example.mylbctest.splashscreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

open class SplashScreenPresenter(view: SplashScreenActivity): SplashScreenMvpContract.Presenter {

    private val TAG: String = "SplashScreenPresenter"

    private var mView: SplashScreenActivity = view
    private var appRoomDatabase = AppRoomDatabase.getInstance(view)
    private var mInteractor: SplashScreenInteractor = SplashScreenInteractor(appRoomDatabase)
    private var disposable: CompositeDisposable = CompositeDisposable()



    override fun loadAllPicturesInBDD() {
        Log.i(TAG, "loadAllPicturesInBDD: début de méthode")

        disposable.add(
            mInteractor.loadAllPicturesInBDD()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i(TAG, "loadAllPicturesInBDD: suscribe() boolean = $it")

                    if (it == true)
                        mView.startPictureActivity()
                }
        )

    }

    override fun clearDisposable() {
        disposable.dispose()
    }

    override fun checkIfDatabaseIsEmpty() {

        disposable.add(
            mInteractor.checkIfDatabaseIsEmpty()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                   if (it)
                       mView.startPictureActivity()
                    else
                        mView.startAlertDialog()
                }
            )
    }


}