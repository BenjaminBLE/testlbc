package com.example.mylbctest.splashscreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.mylbctest.R
import com.example.mylbctest.albumscreen.AlbumActivity
import com.example.mylbctest.utils.AppContext
import com.example.mylbctest.utils.NetworkState

open class SplashScreenActivity : AppCompatActivity(), SplashScreenMvpContract.View {

    private lateinit var mPresenter: SplashScreenPresenter
    private val TAG: String = "SplashScreenActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        AppContext.setContext(applicationContext)

        mPresenter = SplashScreenPresenter(this)

        if (NetworkState.isNetworkAvailable()) {
            mPresenter.loadAllPicturesInBDD()
        } else {
            mPresenter.checkIfDatabaseIsEmpty()
        }



    }

    override fun startPictureActivity() {
        val intent = Intent(this, AlbumActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun showErrorMessage(message: String) {
        Log.i(TAG, "showErrorMessage: ")
    }

    override fun startAlertDialog() {
        Log.i(TAG, "startAlertDialog: Affichage de l'alerteDialog")

        AlertDialog.Builder(this)
            .setTitle("Erreur")
            .setMessage("Une connexion internet est requise pour le premier chargement des données.\nVeuillez activer internet pour les télécharger")
            .setPositiveButton(R.string.close_app) {
                    dialog, which -> finish()
            } .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearDisposable()
    }
}