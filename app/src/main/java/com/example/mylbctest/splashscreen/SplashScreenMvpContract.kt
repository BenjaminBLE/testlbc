package com.example.mylbctest.splashscreen

import io.reactivex.rxjava3.core.Observable

interface SplashScreenMvpContract {

    interface View {
        fun startPictureActivity()
        fun showErrorMessage(message: String)
        fun startAlertDialog()
    }

    interface Presenter {
        fun loadAllPicturesInBDD()
        fun clearDisposable()
        fun checkIfDatabaseIsEmpty()
    }

    interface Interactor {
        fun loadAllPicturesInBDD(): Observable<Boolean>
        fun checkIfDatabaseIsEmpty(): Observable<Boolean>

    }

}