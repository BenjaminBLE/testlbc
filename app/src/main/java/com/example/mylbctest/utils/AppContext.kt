package com.example.mylbctest.utils

import android.content.Context

abstract class AppContext {

    companion object {
        private lateinit var context: Context

        fun setContext(context: Context) {
            this.context = context
        }

        fun getContext(): Context {
            return this.context
        }
    }
}