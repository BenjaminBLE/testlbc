package com.example.mylbctest.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.Toast
import com.example.mylbctest.R

class NetworkState(private val activity: Activity): ConnectivityManager.NetworkCallback() {

    private val TAG = "NetworkState"
    private var isShowToast = false

    override fun onCapabilitiesChanged(network: Network, networkCapabilities: NetworkCapabilities) {
        super.onCapabilitiesChanged(network, networkCapabilities)
        Log.i(TAG, "onCapabilitiesChanged: le réseau est activé")
        if (isShowToast) {
            Toast.makeText(activity.applicationContext, R.string.connectivity_available, Toast.LENGTH_LONG ).show()
            isShowToast = false
        }

    }

    override fun onLost(network: Network) {
        super.onLost(network)
        Log.i(TAG, "onLost: le réseau est perdu")
        Toast.makeText(activity.applicationContext, R.string.connectivity_lost, Toast.LENGTH_LONG ).show()
        isShowToast= true
    }

    companion object {
        val cm = AppContext.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        fun isNetworkAvailable(): Boolean {
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
                    val cap = cm.getNetworkCapabilities(cm.activeNetwork) ?: return false
                    return cap.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                    val networks = cm.allNetworks
                    for (n in networks) {
                        val nInfo = cm.getNetworkInfo(n)
                        if (nInfo != null && nInfo.isConnected) return true
                    }
                }
                else -> {
                    val networks = cm.allNetworkInfo
                    for (nInfo in networks) {
                        if (nInfo != null && nInfo.isConnected) return true
                    }
                }
            }
            return false
        }
    }

}