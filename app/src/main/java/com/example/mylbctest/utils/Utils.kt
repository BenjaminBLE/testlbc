package com.example.mylbctest.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.webkit.WebSettings
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.FutureTarget
import com.bumptech.glide.request.target.Target
import com.example.mylbctest.R
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.model.Album
import com.example.mylbctest.model.Picture
import com.example.mylbctest.model.PictureRetrofit
import java.io.ByteArrayOutputStream
import java.util.*


object Utils {

    var ALBUM_ID: String = "album_id"
    var ALBUM_IS_SAVE = "album_is_save"
    var PICTURE_ID = "album_is_save"

    private var TAG: String = "Utils"

    fun pictureRetrofitToPicture(pictureRetrofitList: List<PictureRetrofit>): List<Picture> {
        val pictureList = ArrayList<Picture>()

        var albumId: Long = 0
        var counterPictureInAlbum: Long = 0
        var album: Album

        for (pictureRetrofit in pictureRetrofitList) {

            Log.i(TAG, "pictureRetrofitToPicture: id : ${pictureRetrofit.id}")

            pictureList.add(
                    Picture(
                            pictureRetrofit.id,
                            pictureRetrofit.albumId,
                            pictureRetrofit.title,
                            pictureRetrofit.url,
                            byteArrayOf(),
                            pictureRetrofit.thumbnailUrl,
                    ))

            if (albumId != pictureRetrofit.albumId) {


                if (albumId >= 1L) {
                    album = Album()

                    val urlThumbnail = GlideUrl(pictureRetrofit.thumbnailUrl, LazyHeaders.Builder()
                            .addHeader("User-Agent", WebSettings.getDefaultUserAgent(AppContext.getContext()))
                            .build())



                    val targetThumbnail: FutureTarget<Bitmap> = Glide.with(AppContext.getContext())
                            .asBitmap()
                            .load(urlThumbnail)
                            .override(410, 410)
                            .error(R.drawable.thumbnail_default)
                            .submit()

                    if (albumId == 1L) {
                        album.albumId = albumId
                    } else {
                        album.albumId = pictureRetrofit.albumId
                    }

                    album.counterPicture = counterPictureInAlbum
                    album.thumbnail = pictureRetrofit.thumbnailUrl

                    try {
                        album.thumbnailByteArray = bitmapToByteArray(targetThumbnail.get())
                    } catch (e: Exception) {
                        Log.e(TAG, "pictureRetrofitToPicture: ")
                        album.thumbnailByteArray = null
                    }

                    AppRoomDatabase.getInstance(AppContext.getContext()).albumDao().insertAlbum(album)
                }

                counterPictureInAlbum = 0
                albumId = pictureRetrofit.albumId

            }

            counterPictureInAlbum++
        }


        return pictureList
    }

    fun byteArrayToBitmap(byteArray: ByteArray?): Bitmap? {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray!!.size)
    }

    fun bitmapToByteArray(bitmap: Bitmap?): ByteArray {
        val stream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    fun loadImageFullByGlide(urlFull: String): Bitmap? {

        val url = GlideUrl(urlFull, LazyHeaders.Builder()
                .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36")
                .build())


        val targetThumbnail: FutureTarget<Bitmap> = Glide.with(AppContext.getContext())
            .asBitmap()
            .load(url)
            .override(Target.SIZE_ORIGINAL)
            .error(R.drawable.full_picture_default)
            .submit()

        return targetThumbnail.get()
    }

    fun loadImageThumbnailByGlide(urlThumbnail: String): Bitmap? {

        val targetThumbnail: FutureTarget<Bitmap> = Glide.with(AppContext.getContext())
            .asBitmap()
            .load(getGlideUrl(urlThumbnail))
            .override(Target.SIZE_ORIGINAL)
            .error(R.drawable.full_picture_default)
            .submit()

        return targetThumbnail?.get()
    }


    fun getGlideUrl(url: String): GlideUrl {

        return GlideUrl(url, LazyHeaders.Builder()
                .addHeader("User-Agent", WebSettings.getDefaultUserAgent(AppContext.getContext()))
                .build())
    }

}