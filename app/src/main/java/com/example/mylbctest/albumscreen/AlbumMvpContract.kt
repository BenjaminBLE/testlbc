package com.example.mylbctest.albumscreen

import com.example.mylbctest.model.AlbumToModelUi
import com.example.mylbctest.model.Picture
import io.reactivex.rxjava3.core.Observable

interface AlbumMvpContract {

    interface View {
        fun showAllAlbum(albumList: List<AlbumToModelUi>)
        fun startProgressDialog()
        fun stopProgressDialog()
    }

    interface Presenter {
        fun loadAllAlbum()
        fun savePictureBitmapByAlbumId(id: Long)
        fun deletePictureBitmapByAlbumId(id: Long)
        fun clearDisposable()
    }

    interface Interactor {
        fun getAllAlbum(): Observable<List<AlbumToModelUi>>
        fun getAllPictureByAlbumId(albumId: Long, isChecked: Boolean): Observable<List<Picture>>
        fun updateBitmapByPictures(pictures: List<Picture>): Observable<Boolean>
        fun deleteBitmapByPictures(pictures: List<Picture>): Observable<Boolean>
    }
}