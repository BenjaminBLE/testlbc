package com.example.mylbctest.albumscreen

import android.util.Log
import com.example.mylbctest.database.AppRoomDatabase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class AlbumPresenter(view: AlbumActivity): AlbumMvpContract.Presenter {

    private val TAG: String = "SplashScreenPresenter"

    private var mView: AlbumActivity = view
    private var appRoomDatabase = AppRoomDatabase.getInstance(view)
    private var mInteractor: AlbumInteractor = AlbumInteractor(this, appRoomDatabase)
    private var disposable: CompositeDisposable = CompositeDisposable()


    override fun loadAllAlbum() {
        Log.i(TAG, "loadAllAlbum: début de la méthode")

        disposable.add(
                mInteractor.getAllAlbum()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe {
                            if (it.isNotEmpty()) {
                                mView.showAllAlbum(it)
                            }
                        })
    }

    override fun savePictureBitmapByAlbumId(id: Long) {
        Log.i(TAG, "loadAllPictureByAlbumId $id : début de la méthode")


        disposable.add(
                mInteractor.getAllPictureByAlbumId(id, true)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe {
                            if (it.isNotEmpty()) {
                                mInteractor.updateBitmapByPictures(it)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io())
                                        .subscribe {
                                            if (it) {
                                                mView.stopProgressDialog()
                                            } else {
                                                mView.stopProgressDialog()
                                                Log.e(TAG, "loadAllPictureByAlbumId: l'enregistrement des image sne s'est pas bien passé", )
                                            }
                                        }
                            } else {
                                Log.e(TAG, "loadAllPictureByAlbumId: la liste de photo est video", )
                                mView.stopProgressDialog()
                            }

                        })


    }

    override fun deletePictureBitmapByAlbumId(id: Long) {
        Log.i(TAG, "deletePictureBitmapByAlbumId $id : début de la méthode")

        disposable.add(
            mInteractor.getAllPictureByAlbumId(id, false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe {
                    if (it.isNotEmpty()) {
                        mInteractor.deleteBitmapByPictures(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe {
                                if (it) {
                                    mView.stopProgressDialog()
                                } else {
                                    mView.stopProgressDialog()
                                    Log.e(TAG, "deletePictureBitmapByAlbumId: la suppression des images ne s'ets pas bien passé", )
                                }
                            }
                    } else {
                        Log.e(TAG, "deletePictureBitmapByAlbumId: la liste de photo est video", )
                        mView.stopProgressDialog()
                    }
                })
    }

    override fun clearDisposable() {
        disposable.dispose()
    }
}