package com.example.mylbctest.albumscreen

import android.util.Log
import com.bumptech.glide.Glide
import com.example.mylbctest.database.AppRoomDatabase
import com.example.mylbctest.model.AlbumToModelUi
import com.example.mylbctest.model.Picture
import com.example.mylbctest.network.PictureServices
import com.example.mylbctest.utils.Utils
import io.reactivex.rxjava3.core.Observable

class AlbumInteractor(presenter: AlbumPresenter, appRoomDatabase: AppRoomDatabase): AlbumMvpContract.Interactor {

    private var mPresenter = presenter
    private var mDatabase = appRoomDatabase
    private val services = PictureServices.PictureServices.create()
    private val TAG: String = "AlbumInteractor"


    override fun getAllAlbum(): Observable<List<AlbumToModelUi>> {
        Log.i(TAG, "getAllAlbum: début de la méthode")

        return Observable.defer {

            val albumToModelUiList = ArrayList<AlbumToModelUi>()
            val albumList = mDatabase.albumDao().getAllAlbum()

            if (albumList.isNotEmpty()) {
                for (album in albumList) {
                    val albumToModelUi = AlbumToModelUi(album.albumId,
                            album.thumbnail,
                            Utils.byteArrayToBitmap(album.thumbnailByteArray),
                            album.counterPicture,
                            album.isChecked)

                    albumToModelUiList.add(albumToModelUi)
                }
            }

            Observable.just(albumToModelUiList)
        }
    }

    override fun getAllPictureByAlbumId(albumId: Long, isChecked: Boolean): Observable<List<Picture>> {
        Log.i(TAG, "getAllPictureByAlbumId: début de la méthode")

        return Observable.defer {

            val album = mDatabase.albumDao().getAlbumById(albumId)
            album.isChecked = isChecked
            mDatabase.albumDao().updateAlbum(album)

            Observable.just(mDatabase.pictureDao().getPicturesByAlbumId(albumId))
        }
    }

    override fun updateBitmapByPictures(pictures: List<Picture>): Observable<Boolean> {
        Log.i(TAG, "updatePictures: début de la méthode")

        return Observable.defer{

            for (picture in pictures) {

                picture.pictureByteArray = Utils.bitmapToByteArray(Utils.loadImageFullByGlide(picture.picture))
                picture.thumbnailByteArray = Utils.bitmapToByteArray(Utils.loadImageThumbnailByGlide(picture.thumbnail))
            }

            val int = mDatabase.pictureDao().updatePictures(pictures)
            if (int != 0) {
                Observable.just(true)
            } else {
                Observable.just(false)
            }

        }


    }

    override fun deleteBitmapByPictures(pictures: List<Picture>): Observable<Boolean> {
        Log.i(TAG, "deleteBitmapByPictures: début de la méthode")

        return Observable.defer{

            for (picture in pictures) {
                picture.thumbnailByteArray = byteArrayOf()
                picture.pictureByteArray = byteArrayOf()

            }

            val int = mDatabase.pictureDao().deleteBitmapByPicture(pictures)
            if (int != 0) {
                Observable.just(true)
            } else {
                Observable.just(false)
            }

        }
    }

}