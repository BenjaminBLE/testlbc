package com.example.mylbctest.albumscreen

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mylbctest.R
import com.example.mylbctest.model.AlbumToModelUi
import com.example.mylbctest.picturescreen.PictureActivity
import com.example.mylbctest.utils.Utils
import java.util.ArrayList

class AlbumAdapter(private val activity: AlbumActivity, private val mPresenter: AlbumPresenter): RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder>() {

    private var mAlbumList: List<AlbumToModelUi> = ArrayList<AlbumToModelUi>()

    fun setData(albumList: List<AlbumToModelUi>) {
        mAlbumList = albumList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.cardview_album, parent, false)
        return AlbumViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {

        val album = mAlbumList[position]

        Glide.with(activity)
                .load(album.thumbnailBitmap)
                .override(410, 410)
                .placeholder(R.drawable.thumbnail_default)
                .into(holder.itemImage)

        holder.itemAlbumId.text = "Album ${album.albumId}"
        holder.itemCounterPicture.text = "${album.counterPicture} photos"

        holder.itemCheckBoxFavorite.isChecked = album.isChecked
        holder.itemCheckBoxFavorite.setOnClickListener {
            if (holder.itemCheckBoxFavorite.isChecked) {
                holder.itemCheckBoxFavorite.setBackgroundResource(R.drawable.ic_baseline_favorite_24)
                activity.startProgressDialog()
                mPresenter.savePictureBitmapByAlbumId(album.albumId)

            } else {
                holder.itemCheckBoxFavorite.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24)
                activity.startProgressDialog()
                mPresenter.deletePictureBitmapByAlbumId(album.albumId, )
            }
        }

        holder.itemCarView.setOnClickListener {
            val intent = Intent(activity, PictureActivity::class.java)
            intent.putExtra(Utils.ALBUM_ID, album.albumId)
            intent.putExtra(Utils.ALBUM_IS_SAVE, holder.itemCheckBoxFavorite.isChecked)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mAlbumList.size
    }


    inner class AlbumViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val itemCarView: CardView = itemView.findViewById(R.id.cardViewAlbumActivityMain)
        val itemImage: ImageView = itemView.findViewById(R.id.imageview_cardview_album)
        val itemAlbumId: TextView = itemView.findViewById(R.id.textview_id_card_album)
        val itemCounterPicture: TextView = itemView.findViewById(R.id.textview_counter_card_album)
        val itemCheckBoxFavorite: CheckBox = itemView.findViewById(R.id.checkbox_favorite_card_album)
    }
}