package com.example.mylbctest.albumscreen

import android.app.ProgressDialog
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mylbctest.databinding.ActivityAlbumBinding
import com.example.mylbctest.model.AlbumToModelUi
import com.example.mylbctest.utils.NetworkState

class AlbumActivity : AppCompatActivity(), AlbumMvpContract.View {

    private lateinit var mPresenter: AlbumPresenter
    private val TAG: String = "AlbumActivity"
    private lateinit var binding: ActivityAlbumBinding
    private lateinit var adapter: AlbumAdapter
    private lateinit var dialog: ProgressDialog
    private val networkState: NetworkState = NetworkState(this)
    private val connectivityManager: ConnectivityManager = NetworkState.cm
    private val networkRequest = NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlbumBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        connectivityManager.requestNetwork(networkRequest, networkState)

        dialog = ProgressDialog(this)

        mPresenter = AlbumPresenter(this)
        mPresenter.loadAllAlbum()

        adapter = AlbumAdapter(this, mPresenter)
        binding.recyclerViewAlbumActivity.adapter = adapter

    }

    override fun showAllAlbum(albumList: List<AlbumToModelUi>) {
        Log.i(TAG, "showAllAlbum: début de la méthode")

        adapter.setData(albumList)
        adapter.notifyDataSetChanged()
    }

    override fun startProgressDialog() {
        dialog.setMessage("Téléchargement des images en cours !")
        dialog.show()
    }

    override fun stopProgressDialog() {
        if (dialog.isShowing) {
            dialog.dismiss();
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.clearDisposable()
    }

}